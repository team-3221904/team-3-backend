// Including basic modules express , knex etc
const express = require('express');
const bodyParser = require('body-parser');
const bcrypt = require('bcrypt');
const knex = require('knex');
const jwt = require('jsonwebtoken');

// Importing route files
const authRoutes = require('./routes/authRoutes');
const locationRoutes = require('./routes/locationRoutes');
const restaurantRoutes = require('./routes/restaurantRoutes');
const bookingRoutes = require('./routes/bookingRoutes');
const slotsRoutes = require('./routes/slotsRoutes');

// Creating an Express app
const app = express();
const port = 3000;
const JWT_SECRET = 'ZoRo2044';

// Initialize knex with the configuration from knexfile.js
const db = knex(require('./knexfile'));

// Middleware to parse JSON in the request body
app.use(bodyParser.json());

// Setting up routes
app.use('/auth', authRoutes);         // Authentication routes
app.use('/location', locationRoutes); // Location-related routes
app.use('/restaurant', restaurantRoutes); // Restaurant-related routes
app.use('/booking', bookingRoutes); 
app.use('/slots', slotsRoutes);   // Booking-related routes

// Middleware to verify the JWT token 
function authenticateToken(req, res, next) {
  const token = req.header('Authorization');

  if (!token) {
    return res.status(401).json({ error: 'Unauthorized' });
  }

  jwt.verify(token, JWT_SECRET, (err, user) => {
    if (err) {
      return res.status(403).json({ error: 'Forbidden' });
    }

    req.user = user;
    next();
  });
}

// Create a new restaurant
app.post('/restaurant', async (req, res) => {
  const { name, image, location, cuisine_type } = req.body;

  if (!name || !location || !cuisine_type) {
    return res.status(400).json({ error: 'Missing required fields' });
  }

  try {
    const newRestaurant = await db('restaurants').insert({ name, image, location, cuisine_type });
    res.status(201).json({ id: newRestaurant[0], name, location, cuisine_type });
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: 'Internal Server Error' });
  }
});

app.get('/api/users/:userId', async (req, res) => {
  const userId = req.params.userId;

  try {
    // Get the last booking made by the user
    const lastBooking = await db('bookings')
      .select('id', 'slot_id', 'customer_id', 'customer_name', 'contact_number', 'booking_date', 'num_guests')
      .where('customer_id', userId)
      .orderBy('created_at', 'desc')
      .limit(1)
      .first();

    if (!lastBooking) {
      return res.status(404).json({ success: false, error: 'No booking found for the user.' });
    }

    // Extract only the date part without the time component
    lastBooking.booking_date = lastBooking.booking_date.toISOString().split('T')[0];

    // Get the start time, end time, and restaurant information using the slot_id
    const slotInfo = await db('slots')
      .select('start_time', 'end_time', 'restaurant_id')
      .where('id', lastBooking.slot_id)
      .first();

    if (!slotInfo) {
      return res.status(404).json({ success: false, error: 'Slot information not found.' });
    }

    // Extract only the `hh:mm` part from start time and end time
    lastBooking.start_time = slotInfo.start_time.slice(0, 5);
    lastBooking.end_time = slotInfo.end_time.slice(0, 5);

    // Get the restaurant name using the restaurant_id
    const restaurantInfo = await db('restaurants')
      .select('name')
      .where('id', slotInfo.restaurant_id)
      .first();

    if (!restaurantInfo) {
      return res.status(404).json({ success: false, error: 'Restaurant information not found.' });
    }

    // Add restaurant name to the response
    lastBooking.restaurant_name = restaurantInfo.name;

    res.status(200).json(lastBooking);
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: 'Internal Server Error' });
  }
});


// Start the server
app.listen(port, () => {
  console.log(`Server is running on http://localhost:${port}`);
});


 // Make a table reservation
/*app.post('/reservation', authenticateToken, async (req, res) => {
  const { restaurantId, startTime, endTime, numberOfPeople, reservationDate, customerName, contactNumber } = req.body;

  if (!restaurantId || !startTime || !endTime || !numberOfPeople || !reservationDate || !customerName || !contactNumber) {
    return res.status(400).json({ error: 'Missing required fields' });
  }

  try {
    // Check if the specified restaurant exists
    const restaurant = await db('restaurants').where({ id: restaurantId }).first();

    if (!restaurant) {
      return res.status(404).json({success:false, error: 'Restaurant not found' });
    }

    // Check if the requested slot exists for the given date and time
    const slot = await db('slots')
      .where({
        restaurant_id: restaurantId,
        start_time: startTime,
        end_time: endTime,
      })
      .first();

    if (!slot) {
      return res.status(400).json({ success:false , error: 'Invalid slot selected' });
    }

    // Check the remaining capacity for the requested slot
    const existingBookings = await db('bookings')
      .where({ slot_id: slot.id, booking_date: reservationDate })
      .select('num_guests');

    const totalReservedGuests = existingBookings.reduce((acc, booking) => acc + booking.num_guests, 0);
    const remainingCapacity = slot.capacity - totalReservedGuests;

    if (numberOfPeople > remainingCapacity) {
      return res.status(400).json({ success:false , error: 'Not enough slots available for the requested number of people' });
    }

    // Create a new booking in the database
    const newBooking = await db('bookings').insert({
      slot_id: slot.id,
      customer_id: req.user.userId,
      customer_name: customerName,
      contact_number: contactNumber,
      booking_date: reservationDate,
      num_guests: numberOfPeople,
    });

    res.status(201).json({
      id: newBooking[0],
      slot_id: slot.id,
      customer_id: req.user.userId,
      customer_name: customerName,
      contact_number: contactNumber,
      booking_date: reservationDate,
      num_guests: numberOfPeople,
    });
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: 'Internal Server Error' });
  }
});

// Get all customers
app.get('/customers', async (req, res) => {
  try {
    const customers = await db('customers').select('*');
    res.json(customers);
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: 'Internal Server Error' });
  }
});

// Create a new customer
app.post('/customers', async (req, res) => {
  const { name, email, password } = req.body;

  if (!name || !email || !password) {
    return res.status(400).json({ error: 'Missing required fields' });
  }

  try {
    const hashedPassword = bcrypt.hashSync(password, 10);   
    const newCustomer = await db('customers').insert({ name, email, password: hashedPassword });
    res.status(201).json({ id: newCustomer[0], name, email });
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: 'Internal Server Error' });
  }   
});

// Get all restaurants
/*app.get('/restaurants', authenticateToken, async (req, res) => {
  try {
    const restaurants = await db('restaurants').select('*');
    res.json(restaurants);
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: 'Internal Server Error' });
  }
});

// Get details for a specific restaurant
app.get('/restaurants/:id', authenticateToken, async (req, res) => {
  const restaurantId = req.params.id;

  try {
    const restaurant = await db('restaurants').where({ id: restaurantId }).first();

    if (!restaurant) {
      return res.status(404).json({ error: 'Restaurant not found' });
    }

    

    res.json(restaurant);
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: 'Internal Server Error' });
  }
});*/