/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> } 
 */
// seeds/01_restaurants_seed.js

// seeds/01_restaurants_seed.js

exports.seed = async function (knex) {
  try {
    // Deletes ALL existing entries
    await knex('restaurants').del();

    // Inserts seed entries for restaurants in Delhi, Gurgaon, and Noida
    await knex('restaurants').insert([
      // Delhi
      {
        name: 'Karim\'s',
        image: 'karims.jpg',
        location: 'Delhi',
        cuisine_type: 'Mughlai',
      },
      {
        name: 'Indian Accent',
        image: 'indian_accent.jpg',
        location: 'Delhi',
        cuisine_type: 'Modern Indian',
      },
      {
        name: 'Dramz',
        image: 'dramz.jpg',
        location: 'Delhi',
        cuisine_type: 'European, North Indian',
      },
      {
        name: 'Saravana Bhavan',
        image: 'saravana_bhavan.jpg',
        location: 'Delhi',
        cuisine_type: 'South Indian',
      },
      {
        name: 'Gali Paranthe Wali',
        image: 'paranthe_wali.jpg',
        location: 'Delhi',
        cuisine_type: 'Street Food',
      },
      
      // Gurgaon
      {
        name: 'Farzi Cafe',
        image: 'farzi_cafe.jpg',
        location: 'Gurgaon',
        cuisine_type: 'Modern Indian',
      },
      {
        name: 'Mamagoto',
        image: 'mamagoto.jpg',
        location: 'Gurgaon',
        cuisine_type: 'Asian',
      },
      {
        name: 'The Grammar Room',
        image: 'grammar_room.jpg',
        location: 'Gurgaon',
        cuisine_type: 'European',
      },
      
      // Noida
      {
        name: 'Barbeque Nation',
        image: 'barbeque_nation.jpg',
        location: 'Noida',
        cuisine_type: 'Barbecue, North Indian',
      },
      {
        name: 'The Ancient Barbeque',
        image: 'ancient_barbeque.jpg',
        location: 'Noida',
        cuisine_type: 'Mughlai, North Indian',
      },
      {
        name: 'Burbee\'s Cafe',
        image: 'burbees_cafe.jpg',
        location: 'Noida',
        cuisine_type: 'Cafe, Italian',
      },
    ]);

    console.log('Restaurants seed data inserted successfully.');
  } catch (error) {
    console.error('Error seeding restaurants:', error);
  }
};
