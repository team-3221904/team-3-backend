/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> } 
 */
exports.seed = async function(knex) {
  // Deletes ALL existing entries
  await knex('table_name').del()
  await knex('table_name').insert([
    {id: 1, colName: 'rowValue1'},
    {id: 2, colName: 'rowValue2'},
    {id: 3, colName: 'rowValue3'}
  ]);
};

// seeds/04_customers_seed.js
const bcrypt = require('bcrypt');

exports.seed = function (knex) {
  // Deletes ALL existing entries
  return knex('customers')
    .del()
    .then(function () {
      // Inserts seed entries
      const hashedPassword = bcrypt.hashSync('password123', 10); // Hash a sample password

      const customers = [
        {
          name: 'Customer A',
          email: 'customer_a@example.com',
          password: hashedPassword,
        },
        {
          name: 'Customer B',
          email: 'customer_b@example.com',
          password: hashedPassword,
        },
        {
          name: 'Customer C',
          email: 'customer_c@example.com',
          password: hashedPassword,
        },
      ];

      return knex('customers').insert(customers);
    });
};

