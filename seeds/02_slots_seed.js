/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> } 
 */
exports.seed = async function(knex) {
  // Deletes ALL existing entries
  await knex('table_name').del()
  await knex('table_name').insert([
    {id: 1, colName: 'rowValue1'},
    {id: 2, colName: 'rowValue2'},
    {id: 3, colName: 'rowValue3'}
  ]);
};

// seeds/02_slots_seed.js

exports.seed = async function (knex) {
  try {
    // Deletes ALL existing entries
    await knex('slots').del();

    // Get the restaurant IDs from the inserted restaurants
    const delhiRestaurants = await knex('restaurants').where('location', 'Delhi');
    const gurgaonRestaurants = await knex('restaurants').where('location', 'Gurgaon');
    const noidaRestaurants = await knex('restaurants').where('location', 'Noida');

    const restaurantIds = [
      ...delhiRestaurants.map(restaurant => restaurant.id),
      ...gurgaonRestaurants.map(restaurant => restaurant.id),
      ...noidaRestaurants.map(restaurant => restaurant.id),
    ];

    // Generate dummy slots data for each restaurant with reduced capacity to 5
    const dummySlots = restaurantIds.flatMap(restaurantId => {
      const slotsForRestaurant = [];

      // Generate 5 dummy slots for each restaurant
      for (let i = 1; i <= 5; i++) {
        slotsForRestaurant.push({
          restaurant_id: restaurantId,
          start_time: `${i}:00`,  // e.g., 2:00, 4:00, 6:00, ...
          end_time: `${i+1}:00`,  // e.g., 4:00, 6:00, 8:00, ...
          capacity: 5, // Reduce the capacity to 5
        });
      }

      return slotsForRestaurant;
    });

    // Insert the modified dummy slots data into the 'slots' table
    await knex('slots').insert(dummySlots);

    console.log('Slots seed data inserted successfully with reduced capacity to 5.');
  } catch (error) {
    console.error('Error seeding slots:', error);
  }
};
