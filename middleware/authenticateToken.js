const jwt = require('jsonwebtoken');
const { JWT_SECRET } = require('../config');

// Middleware to verify the jwt token
function authenticateToken(req, res, next) {
    const token = req.header('Authorization');
  
    if (!token) {
      return res.status(401).json({ error: 'Unauthorized' });
    }
  
    jwt.verify(token, JWT_SECRET, (err, user) => {
      if (err) {
        return res.status(403).json({ error: 'Forbidden' });
      }
  
      req.user = user;
      next();
    });
}

module.exports = authenticateToken;
