/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.up = function(knex) {
  
};

/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.down = function(knex) {
  
};

exports.up = function (knex) {
    return knex.schema.createTable('inventories', function (table) {
      table.increments('id').primary();
      table.integer('restaurant_id').unsigned().references('id').inTable('restaurants');
      table.integer('slot_id').unsigned().references('id').inTable('slots');
      table.integer('quantity');
      table.timestamps(true, true);
    });
  };
  
  exports.down = function (knex) {
    return knex.schema.dropTable('inventories');
  };
  