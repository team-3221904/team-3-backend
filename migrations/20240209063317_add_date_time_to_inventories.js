/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.up = function(knex) {
  
};

/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.down = function(knex) {
  
};

//migrations/20240209123456_add_date_time_to_inventories.js 

exports.up = function (knex) {
    return knex.schema.table('inventories', function (table) {
      table.dateTime('Date').defaultTo(knex.fn.now());
      
    });
  };
  
  exports.down = function (knex) {
    return knex.schema.table('inventories', function (table) {
      table.dropColumn('created_at');
      table.dropColumn('updated_at');
    });
  };
   