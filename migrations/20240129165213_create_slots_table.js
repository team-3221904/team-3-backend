/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.up = function(knex) {
  
};

/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.down = function(knex) {
  
};


// migrations/create_slots_table.js

exports.up = function(knex) {
    return knex.schema.createTable('slots', function(table) {
      table.increments('id').primary();
      table.integer('restaurant_id').unsigned().notNullable();
      table.foreign('restaurant_id').references('id').inTable('restaurants');
      table.time('start_time').notNullable();
      table.time('end_time').notNullable();
      table.integer('capacity').notNullable();
      table.timestamps(true, true); // Adds created_at and updated_at columns
    });
  };
  
  exports.down = function(knex) {
    return knex.schema.dropTable('slots');
  };
  