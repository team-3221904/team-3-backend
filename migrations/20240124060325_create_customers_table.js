/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.up = function(knex) {
  
};

/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.down = function(knex) {
  
};


// migrations/yyyy_create_customers_table.js
exports.up = function (knex) {
    return knex.schema.createTable('customers', function (table) {
      table.increments('id').primary();
      table.string('name');
      table.string('email').unique();
      table.string('password'); // Assume this column is for storing hashed passwords
      table.timestamps(true, true); // Adds created_at and updated_at columns
    });
  };
  
  exports.down = function (knex) {
    return knex.schema.dropTable('customers');
  };
  
