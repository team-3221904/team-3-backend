/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.up = function(knex) {
  
};

/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.down = function(knex) {
  
};


// migrations/yyyy_add_contact_number_to_customers.js
exports.up = function (knex) {
    return knex.schema.table('customers', function (table) {
      table.string('contactNumber').unique();
    });
  };
  
  exports.down = function (knex) {
    return knex.schema.table('customers', function (table) {
      table.dropColumn('contactNumber');
    });
  };
  