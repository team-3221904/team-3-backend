/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.up = function(knex) {
  
};

/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.down = function(knex) {
  
};


// migrations/xxxx_create_bookings_table.js

exports.up = function (knex) {
    return knex.schema.createTable('bookings', function (table) {
      table.increments('id').primary();
      table.integer('slot_id').unsigned().references('id').inTable('slots');
      table.integer('customer_id').unsigned().references('id').inTable('customers');
      table.string('customer_name').notNullable();
      table.string('contact_number').notNullable();
      table.date('booking_date').notNullable();
      table.integer('num_guests').notNullable();
      table.timestamp('created_at').defaultTo(knex.fn.now());
      table.timestamp('updated_at').defaultTo(knex.fn.now());
    });
  };
  
  exports.down = function (knex) {
    return knex.schema.dropTable('bookings');
  };
  