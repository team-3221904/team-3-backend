/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.up = function(knex) {
  
};

/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.down = function(knex) {
  
};

// migrations/xxxx_create_restaurants_table.js
exports.up = function (knex) {
    return knex.schema.createTable('restaurants', function (table) {
      table.increments('id').primary();
      table.string('name');
      table.string('image');
      table.string('location');
      table.string('cuisine_type');
      table.timestamps(true, true); // Adds created_at and updated_at columns
    });
  };
  
  exports.down = function (knex) {
    return knex.schema.dropTable('restaurants');
  };
  