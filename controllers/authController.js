const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const knex = require('knex');
const db = knex(require('../knexfile')); // Importing the Knex instance

const JWT_SECRET = 'ZoRo2044';

// Function to handle user login
async function login(req, res) {
  const { email, password } = req.body;

  // Validate input fields
  if (!email || !password) {
    return res.status(400).json({ error: 'Missing email or password' });
  }

  // Validate email format
  const emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
  if (!emailRegex.test(email)) {
    return res.status(400).json({ error: 'Invalid email format' });
  }

  try {
    // Fetch user from the database based on email
    const user = await db('customers').where({ email }).first();

    // Check if user exists
    if (!user) {
      return res.status(401).json({ error: 'User not found' });
    }

    // Compare hashed password
    const passwordMatch = await bcrypt.compare(password, user.password);

    // Check if password is correct
    if (!passwordMatch) {
      return res.status(401).json({ error: 'Incorrect password' });
    }

    // Generate JWT token for the user
    const token = jwt.sign(
      { userId: user.id, email: user.email },
      JWT_SECRET,
      { expiresIn: '21h' }
    );

    // Respond with success and token
    res.status(200).json({
      message: 'Login successful',
      token,
      user: {
        id: user.id,
        name: user.name,
        email: user.email,
        contactNumber: user.contactNumber,
      },
    });

  } catch (error) {
    console.error(error);
    res.status(500).json({ error: 'Internal Server Error' });
  }
}

// Function to handle user signup
async function signup(req, res) {
  const { name, email, password, contactNumber } = req.body;

  // Validate required fields
  if (!name || !email || !password || !contactNumber) {
    return res.status(400).json({ error: 'Missing required fields' });
  }

  // Validate email format
  const emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
  if (!emailRegex.test(email)) {
    return res.status(400).json({ error: 'Invalid email format' });
  }

  // Validate password length
  if (password.length < 8) {
    return res.status(400).json({ error: 'Password must be at least 8 characters long' });
  }

  // Validate contact number format
  const contactNumberRegex = /^\d{10}$/;
  if (!contactNumberRegex.test(contactNumber)) {
    return res.status(400).json({ error: 'Invalid contact number format' });
  }

  try {
    // Check if user with the given email or contact number already exists
    const existingUser = await db('customers').where({ email }).orWhere({ contactNumber }).first();

    // If user already exists, return error
    if (existingUser) {
      return res.status(409).json({ error: 'User already exists with this email or contact number' });
    }

    // Hash the password
    const hashedPassword = await bcrypt.hash(password, 10);

    // Create a new user in the database
    const [newUserId] = await db('customers').insert({
      name,
      email,
      password: hashedPassword,
      contactNumber,
    });

    // Generate JWT token for the new user
    const token = jwt.sign(
      { userId: newUserId, email },
      JWT_SECRET,
      { expiresIn: '21h' }
    );

    // Respond with success and token
    res.status(201).json({
      message: 'Sign-up successful',
      token,
      user: {
        id: newUserId,
        name,
        email,
        contactNumber,
      },
    });
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: 'Internal Server Error' });
  }
}

// Exporting the functions
module.exports = {
  login,
  signup,
};
