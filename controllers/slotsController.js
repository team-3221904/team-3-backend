const knex = require('knex');
const config = require('../knexfile');
const db = knex(config);
const jwt = require('jsonwebtoken');

async function getAvailableSlots(req, res) {
  const { restaurantId, date } = req.query;

  if (!restaurantId || !date) {
    return res.status(400).json({ error: 'Both restaurantId and date are required parameters.' });
  }

  try {
    // Get all slots for the given restaurantId
    const slots = await db('slots')
      .where({
        'restaurant_id': restaurantId,
      })
      .select('id', db.raw('DATE_FORMAT(start_time, "%H:%i") as start_time'), db.raw('DATE_FORMAT(end_time, "%H:%i") as end_time'), 'capacity');

    // Get inventories for the specified date and restaurantId
    const inventories = await db('inventories')
      .where({
        'date': date,
        'restaurant_id': restaurantId,
      })
      .select('slot_id', 'quantity');

    // Create a map to efficiently lookup inventory quantities by slot_id
    const inventoryMap = inventories.reduce((acc, inventory) => {
      acc[inventory.slot_id] = inventory.quantity;
      return acc;
    }, {});

    // Filter out slots with inventory quantity === 0
    const availableSlots = slots
      .filter((slot) => inventoryMap[slot.id] !== 0 || inventoryMap[slot.id] === undefined)
      .map((slot) => {
        return {
          slot_id: slot.id,
          start_time: slot.start_time,
          end_time: slot.end_time,
          remaining_capacity: inventoryMap[slot.id] !== undefined ? inventoryMap[slot.id] : slot.capacity,
        };
      });

    res.json({ availableSlots });
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: 'Internal Server Error' });
  }
}

module.exports = {
  getAvailableSlots,
};
