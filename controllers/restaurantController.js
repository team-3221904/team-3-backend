const knex = require('knex');
const config = require('../knexfile');
const db = knex(config);
const jwt = require('jsonwebtoken');

// Function to get all restaurants
async function getAllRestaurants(req, res) {
  try {
    const restaurants = await db('restaurants').select('*');
    res.json(restaurants);
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: 'Internal Server Error' });
  }
}

// Function to get a restaurant by ID
async function getRestaurantById(req, res) {
  const restaurantId = req.params.id;

  try {
    const restaurant = await db('restaurants').where({ id: restaurantId }).first();

    if (!restaurant) {
      return res.status(404).json({ error: 'Restaurant not found' });
    }

    res.json(restaurant);
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: 'Internal Server Error' });
  }
}

// Exporting the functions
module.exports = {
  getAllRestaurants,
  getRestaurantById,
};
