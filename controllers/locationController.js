const knex = require('knex');
const config = require('../knexfile');
const db = knex(config);
const jwt = require('jsonwebtoken');

async function getLocations(req, res) {
  try {
    // Query the database to retrieve all unique locations
    const locations = await db('restaurants').distinct('location').pluck('location');
    res.json(locations);
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: 'Internal Server Error' });
  }
}

async function getRestaurantsByLocation(req, res) {
  try {
    // Extract the 'place' route parameter from the request
    const place = req.params.place.replace(/['"]/g, '');
    console.log('Received place:', place);

    let query = db('restaurants');
    if (place) {
      // Modify the condition to search for '/delhi'
      query = query.where('location', 'like', `%${place}`);
    }

    // Log the generated SQL query
    console.log('Generated SQL Query:', query.toString());

    // Execute the query and retrieve the restaurants 
    const restaurants = await query.select('*');
    console.log('Matching restaurants:', restaurants);

    res.json(restaurants);
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: 'Internal Server Error' });
  }
}

module.exports = {
  getLocations,
  getRestaurantsByLocation,
};
