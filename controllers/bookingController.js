const knex = require('knex');
const config = require('../knexfile');
const db = knex(config);
const jwt = require('jsonwebtoken');

//validating date 
function isValidDate(dateString) {
  const regex = /^\d{4}-\d{2}-\d{2}$/;
  if (!regex.test(dateString)) return false;

  const date = new Date(dateString + 'T00:00:00Z');
  const currentDate = new Date();

  // Extract only the date part without the time component for comparison
  date.setUTCHours(0, 0, 0, 0);
  currentDate.setUTCHours(0, 0, 0, 0);

  return date >= currentDate; // Ensure the date is today or in the future
}


async function makeReservation(req, res) {
  const { restaurantId, startTime, endTime, numberOfPeople, reservationDate, customerName, contactNumber } = req.body;

  // Validate required fields
  if (!restaurantId || !startTime || !endTime || !numberOfPeople || !reservationDate || !customerName || !contactNumber) {
    return res.status(400).json({ error: 'Missing required fields' });
  }

  // Validate startTime and endTime format (you might need a more specific format validation)
  const timeRegex = /^(0[0-9]|1[0-9]|2[0-3]):[0-5][0-9]$/;
  if (!timeRegex.test(startTime) || !timeRegex.test(endTime)) {
    return res.status(400).json({ error: 'Invalid time format for startTime or endTime' });
  }

  // Check if startTime is before endTime
  if (startTime >= endTime) {
    return res.status(400).json({ error: 'startTime must be before endTime' });
  }

  // Validate numberOfPeople is a positive integer
  if (!Number.isInteger(numberOfPeople) || numberOfPeople <= 0) {
    return res.status(400).json({ error: 'numberOfPeople must be greater than 0' });
  }

  // Validate reservationDate format and future date
  if (!isValidDate(reservationDate)) {
    return res.status(400).json({ error: 'Invalid date format or past date for reservationDate' });
  }

  try {
    // Check if the specified restaurant exists
    const restaurant = await db('restaurants').where({ id: restaurantId }).first();

    if (!restaurant) {
      return res.status(404).json({ success: false, error: 'Restaurant not found' });
    }
    
    // Check if the requested slot exists for the given date and time
    const slot = await db('slots')
      .where({
        restaurant_id: restaurantId,
        start_time: startTime,
        end_time: endTime,
      })
      .first();

    if (!slot) {
      return res.status(400).json({ success: false, error: 'Invalid slot selected' });
    }

    // Check the remaining capacity for the requested slot
    const existingBookings = await db('bookings')
      .where({ slot_id: slot.id, booking_date: reservationDate })
      .select('num_guests');

    const totalReservedGuests = existingBookings.reduce((acc, booking) => acc + booking.num_guests, 0);
    const remainingCapacity = slot.capacity - totalReservedGuests;
    const quant = remainingCapacity - numberOfPeople;

    if (numberOfPeople > remainingCapacity) {
      return res.status(400).json({ success: false, error: 'Not enough slots available for the requested number of people' });
    }

    // Create a new booking in the database
    const newBooking = await db('bookings').insert({
      slot_id: slot.id,
      customer_id: req.user.userId,
      customer_name: customerName,
      contact_number: contactNumber,
      booking_date: reservationDate,
      num_guests: numberOfPeople,
    });

    // Update the inventory table with the remaining capacity
    const inventoryEntry = await db('inventories').insert({
      slot_id: slot.id,
      restaurant_id: restaurantId,
      quantity: quant,
      created_at: new Date(),
      updated_at: new Date(),
      date: reservationDate,
    });

    res.status(201).json({
      id: newBooking[0],
      slot_id: slot.id,
      customer_id: req.user.userId,
      customer_name: customerName,
      contact_number: contactNumber,
      booking_date: reservationDate,
      num_guests: numberOfPeople,
    });
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: 'Internal Server Error' });
  }
}

module.exports = {
  makeReservation,
};
