//restaurantRoutes.js
const express = require('express');
const restaurantController = require('../controllers/restaurantController');
const authenticateToken = require('../middleware/authenticateToken'); // Assuming you have the middleware folder

const router = express.Router();

// GET endpoint to get all restaurants
router.get('/restaurants', authenticateToken, restaurantController.getAllRestaurants);

// GET endpoint to get details for a specific restaurant
router.get('/restaurants/:id', authenticateToken, restaurantController.getRestaurantById);

module.exports = router;
