// bookingRoutes.js
const express = require('express');
const bookingController = require('../controllers/bookingController');
const authenticateToken = require('../middleware/authenticateToken');

const router = express.Router();

// POST endpoint for making a reservation
router.post('/makeReservation', authenticateToken, bookingController.makeReservation);

module.exports = router;
