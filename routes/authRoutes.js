const express = require('express');
const authController = require('../controllers/authController');

const router = express.Router();

// POST endpoint for user login
router.post('/login', authController.login);

// POST endpoint for user signup
router.post('/signup', authController.signup);

module.exports = router;
