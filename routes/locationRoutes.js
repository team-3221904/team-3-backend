//locationRoutes.js
const express = require('express');
const locationController = require('../controllers/locationController');
const authenticateToken = require('../middleware/authenticateToken'); // Import the middleware

const router = express.Router();

// GET endpoint to retrieve all unique locations from the 'restaurants' table
router.get('/locations', authenticateToken, locationController.getLocations);

// GET endpoint to retrieve restaurants by location
router.get('/locations/:place', authenticateToken, locationController.getRestaurantsByLocation);

module.exports = router;
