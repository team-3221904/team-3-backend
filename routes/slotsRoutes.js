const express = require('express');
const slotsController = require('../controllers/slotsController');

const router = express.Router();

// Route for getting available slots
router.get('/available-slots', slotsController.getAvailableSlots);

module.exports = router;
